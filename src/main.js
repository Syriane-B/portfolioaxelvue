import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import "./assets/app.scss";
Vue.use(BootstrapVue);
Vue.config.productionTip = false;

new Vue({
  el: "#app",
  store,
  render: h => h(App)
}).$mount("#app");
